'use strict'

const Sequelize = require('database/sequelize')

// Extract database configuration information
const { username, password, database, host, dialect } = require('./config.js')

// Create sequelize instance
const sequelize = new Sequelize(database, username, password, {
  logging: null,
  host,
  dialect
})

console.log(username, database)

sequelize.authenticate().then(() => {
  console.log('Database: Connection has been established successfully.')
}).catch(err => {
  console.error('Unable to connect to the database:', err)
})

module.exports = sequelize
