'use strict'

module.exports = (sequelize, DataTypes) => {
  const Memo = sequelize.define('Memo', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4
    },
    header: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    body: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE
    },
    updatedAt: {
      type: DataTypes.DATE
    }
  }, {
    freezeTableName: true,
    tableName: 'Memo',
    timestamps: true,
    paranoid: false,
    classMethods: {
      associate (models) {
        // associations can be defined here
      }
    }
  })

  return Memo
}
