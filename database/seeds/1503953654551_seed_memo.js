'use strict'

/**
 * Seed_Memo
 * @param db {Database}
 * @param faker
 * @return {Promise}
 */
module.exports = function (db, faker) {
  const { Memo } = db.models

  const data = []

  for (let i = 0; i < 100000; i++) {
    data[i] = {
      "header": `memo ${i}`,
      "body": "pour faire ca tape ca"
    }
  }

  return Memo.bulkCreate(data)
}
