'use strict'

/**
 * UP - Altering commands
 *
 * @param {QueryInterface} query
 * @param {DataTypes} DataTypes
 * @returns {Promise}
 */
exports.up = function (query, DataTypes) {
  query.createTable('Memo', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4
    },
    header: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    body: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE
    },
    updatedAt: {
      type: DataTypes.DATE
    }
  })
}

/**
 * DOWN - Reverting command
 *
 * @param query
 * @param DataTypes
 * @returns {Promise}
 */
exports.down = function (query, DataTypes) {
  query.dropTable('Memo')
}
