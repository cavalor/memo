'use strict'

const models = Symbol('database:models')
const sequelize = Symbol('database:sequelize')

module.exports = class Database {
  constructor (sequelizeInstance) {
    this[models] = {}
    this[sequelize] = sequelizeInstance
  }

  /**
   * Return all models defined in the database instance context
   * @return {Object} models list
   */
  get models () {
    return this[models]
  }

  /**
   * Return the sequelize instance
   * @return {Sequelize}
   */
  get sequelize () {
    return this[sequelize]
  }

  /**
   * Set a model in the "models" map
   * @param {Sequelize.Model}
   */
  setModel (model) {
    this.models[model.name] = model
  }

  /**
   * Get a model by name
   * @return {Sequelize.Model}
   */
  getModel (name) {
    return this.models.hasOwnProperty(name) ? this.models[name] : null
  }
}
