# -*- mode: ruby -*-
# vi: set ft=ruby :

virtual_machines = [
  { "name" => "memo", "ip" => "192.168.0.50" }
]

Vagrant.configure("2") do |config|
  virtual_machines.each do |opts|
    config.vm.define opts["name"] do |node|
      ############################################################
      # Base
      ###
      node.vm.box = 'debian/stretch64'
      node.vm.hostname = opts["name"]
      ############################################################
      # VM provider configuration (virtualbox)
      ###
      node.vm.provider :virtualbox do |vm|
        vm.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
        vm.customize ["modifyvm", :id, "--memory", 1024]
        vm.customize ["modifyvm", :id, "--cpus", 4]
        vm.customize ["modifyvm", :id, "--cpuexecutioncap", "20"]
        vm.customize ["modifyvm", :id, "--name", opts["name"]]
      end
      ############################################################
      # Network & port forwarding
      ###
      node.vm.network "private_network", ip: opts["ip"]
      ############################################################
      # Synced folders
      ###
      node.vm.synced_folder ".", "/data", type: "sshfs"
      ############################################################
      # Add host ssh public key in .ssh/authorized_keys
      ###
      ssh_pub_key = File.readlines("#{Dir.home}/.ssh/id_rsa.pub").first.strip
      node.vm.provision "shell" do |s|
        s.inline = <<-SHELL
          mkdir /root/.ssh
          echo #{ssh_pub_key} >> /root/.ssh/authorized_keys
          echo #{ssh_pub_key} >> /home/vagrant/.ssh/authorized_keys
        SHELL
      end
    end
  end
end