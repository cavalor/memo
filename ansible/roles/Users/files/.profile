# SSH
alias forget_host="ssh-keygen -R"

# xclip alias 
alias pbcopy="xclip -sel clip"

# copy ssh public key shortcut
alias cpkey="pbcopy < ~/.ssh/id_rsa.pub"

# nvm
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"

