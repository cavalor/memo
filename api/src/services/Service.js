import boom from 'boom'

export default class Service {
  constructor (server) {
    this.app = server
    this.errors = boom
  }
}