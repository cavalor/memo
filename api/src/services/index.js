import fs from 'fs'
import path from 'path'
import server from '../index'

const filter = f => (f.indexOf('.') !== 0 && f !== 'index.js' && f !== 'Service.js')

const getPath = fileName => path.join(__dirname, fileName)

function register (server, options, next) {
  const services = {}

  const servicePaths = fs.readdirSync(__dirname)
    .filter(filter)
    .map(getPath)


  for (const servicePath of servicePaths) {
    const Service = require(servicePath).default

    if (!Service.name) {
      throw TypeError('Service should be a class')
    }

    services[Service.name] = new Service(server)
  }

  server.decorate('request', 'service', services)
  server.decorate('server', 'service', services)

  next()
}

register.attributes = {
  name: 'services'
}

export default { register }
