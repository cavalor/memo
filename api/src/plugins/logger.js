import good from 'good' 

export default { 
  register: good,
  options: {
    ops: {
      interval: 1000
    },
    reporters: {
      myConsoleReporter: [{
        module: 'good-squeeze',
        name: 'Squeeze',
        args: [{ log: '*', response: '*' }]
      }, {
        module: 'good-console'
      }, 'stdout']
      /*
      myFileReporter: [{
        module: 'Good-squeeze',
        name: 'Squeeze',
        args: [{ ops: '*' }]
      }, {
        module: 'Good-squeeze',
        name: 'SafeJson'
      }, {
        module: 'Good-file',
        args: ['./logs/awesome_log']
      }],
      myHTTPReporter: [{
        module: 'Good-squeeze',
        name: 'Squeeze',
        args: [{ error: '*' }]
      }, {
        module: 'Good-http',
        args: ['http://prod.logs:3000', {
          wreck: {
            headers: { 'x-api-key': 12345 }
          }
        }]
      }]
      */
    }
  }
}
