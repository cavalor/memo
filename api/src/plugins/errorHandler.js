import Boom from 'boom'

function register (server, options, next) {
  const handleError = err => {
    if (err.isBoom) {
      return err
    }
    
    return Boom.internal(err)
  }

  // Decorate request and server objects with Database instance
  server.decorate('request', 'handleError', handleError)
  server.decorate('server', 'handleError', handleError)

  next()
}


// Set the plugins attributes
register.attributes = {
  name: 'handle-error'
}

export default { register }
