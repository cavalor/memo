import database from '../../../database'


function register (server, options, next) {
  server.decorate('server', 'database', database)
  
  next()
}

register.attributes = {
  name: 'auth-wrapper'
}

export default { register }
