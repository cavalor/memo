import AuthJWT from 'hapi-auth-jwt2'
import {JWT_SECRET} from '../constants'

function validateFunc (decoded, request, callback) {
  return callback(null, true)
}

function register (server, options, next) {
  server.register(AuthJWT)

  server.auth.strategy('jwt', 'jwt', true, {
    key: JWT_SECRET,
    validateFunc,
    verifyOptions: {algorithms: ['HS256']}
  })
  
  next()
}

register.attributes = {
  name: 'auth-wrapper'
}

export default { register }
