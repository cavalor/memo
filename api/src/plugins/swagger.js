import swagger from 'hapi-swagger'

export default {
  register: swagger,
  options: {
    cors: true,
    info: {
      'title': 'MEMO API',
      'version': '0.0.1'
    }
  }
}

