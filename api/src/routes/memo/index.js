import * as handlers from './handlers'
import joi from 'joi'

const prefix = '/memo'

export default [
  {
    path: prefix,
    method: 'GET',
    config: {
      handler: handlers.getList,
      auth: false
    }
  },
  {
    path: prefix,
    method: 'POST',
    config: {
      handler: handlers.addItem,
      auth: false,
      validate: {
        payload: {
          header: joi.string().required(),
          body: joi.string().required()
        }
      }
    }
  }
]