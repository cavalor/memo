import redis from 'redis'
import bluebird from 'bluebird'

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const {Memo} = database.models
const cache = redis.createClient()

export function getList (request, reply) {
  const hrstart = process.hrtime()

  const end = () => {
    const hrend = process.hrtime(hrstart)
    reply(`Execution time (hr): ${hrend[0]}s ${hrend[1]/1000000}ms"`)
  }

  cache.get('memos', (err, values) => {
    if (values) {
      end()
    } else {
      Memo.findAndCountAll().then(({rows}) => {
        const memos = rows.map(el => el.dataValues)
        cache.set('memos', memos)
        end()
      })
    }
  })
}

export function addItem (request, reply) {
  Memo.create(request.payload).then(memo => {
    reply(memo)
  })
}
