import fs from 'fs'
import path from 'path'

function register (server, options, next) {
  const filter = file => ((file.indexOf('.') !== 0) && (file.slice(-3) !== '.js'))

  const endpoints = fs.readdirSync(__dirname)
    .filter(filter)
    .map(f => path.resolve(__dirname, f))

  for (const endpoint of endpoints) {
    const routes = require(endpoint).default
    
    server.route(routes)
  }

  next()
}

register.attributes = {
  name: 'routes',
  version: '1.0.0'
}

export default { register }
