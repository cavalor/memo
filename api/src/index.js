import hapi from 'hapi' // hapi framework
import database from '../../database'

global.database = database

const server = new hapi.Server()

module.exports = server

process.on('uncaughtException', (err) => {
  console.trace(err)
})

// Declare connections
server.connection({
  port: 3000,
  router: {
    stripTrailingSlash: true
  }
})

// Register plugins
// Imports with '.default' is for es6 style exports
server.register([
  require('./plugins/auth').default,
  require('./plugins/errorHandler').default,
  require('./plugins/logger').default,
  require('./plugins/swagger').default,
  require('inert'),
  require('vision'),
  require('blipp'),
  require('./services/index').default,
  require('./routes/index').default
], (err) => {
  if (err){
    console.trace(err)
  }

  const simulateLatency = false

  if (simulateLatency) {
    server.ext('onPreResponse', (request, reply) => {
      setTimeout(() => {
        reply.continue()
      }, 500)
    })
  }

  server.ext('onPreResponse', require('./middlewares/cors').default)

  server.start()
})
